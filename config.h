/* This file was generated with: ./configure --prefix=/usr --securedir=/lib/x86_64-linux-gnu/security */
/* Do not edit manually. */

/* name of variable to set in environment */
#define VAR_NAME            "XDG_RUNTIME_DIR"

/* parent dir for runtime dirs */
#define PARENT_DIR          "/run/user"

